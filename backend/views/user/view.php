<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'name',
            'surname',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'email:email',
            'status',
            'created_at',
            'updated_at',
//            'verification_token',
        ],
    ]) ?>

    <h3 class="text-center">Reviews</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Game Name</th>
            <th>Rate</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->reviews as $purchase): ?>
            <tr>
                <td><?= Html::a(Html::encode($purchase->game->name),
                        Url::to(['/game/view', 'id' => $purchase->game_id])); ?></td>
                <td><?= $purchase->rating ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <h3 class="text-center">Purchases</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Game Name</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->purchases as $purchase): ?>
            <tr>
                <td><?= Html::a(Html::encode($purchase->game->name),
                        Url::to(['/purchase/view', 'id' => $purchase->game_id])); ?></td>
                <td><?= $purchase->amount ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

</div>
