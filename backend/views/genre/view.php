<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Genre */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Genres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="genre-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->genre_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->genre_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'genre_id',
            'name',
            'description:ntext',
        ],
    ]) ?>

    <table class="table">
        <thead>
            <tr>
                <th>Game Name</th>
                <th>Price</th>
                <th>Release Date</th>
            </tr>
        </thead>
        <tbody>
        <? foreach ($model->genreGames as $genreGame): ?>
            <tr>
                <td><?= Html::a(Html::encode($genreGame->game->name),
                        Url::to(['/game/view', 'id' => $genreGame->game_id])); ?></td>
                <td><?= $genreGame->game->price ?></td>
                <td><?= $genreGame->game->release_date ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

</div>
