<?php

use common\models\Company;
use common\models\Game;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyGame */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-game-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Company::find()->all(),
    'company_id', 'name')) ?>

    <?= $form->field($model, 'game_id')->dropDownList(ArrayHelper::map(Game::find()->all(),
    'game_id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
