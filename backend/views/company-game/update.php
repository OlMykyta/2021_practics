<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyGame */

$this->title = 'Update Company Game: ' . $model->company_game_id;
$this->params['breadcrumbs'][] = ['label' => 'Company Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->company_game_id, 'url' => ['view', 'id' => $model->company_game_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-game-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
