<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyGame */

$this->title = 'Create Company Game';
$this->params['breadcrumbs'][] = ['label' => 'Company Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-game-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
