<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CompanyGameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-game-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Company Game', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'company_game_id',
            [
                'attribute' => 'company_id',
                'format' => 'raw',
                'value' => function(\common\models\CompanyGame $companyGame) {
                    return Html::a($companyGame->company->name, \yii\helpers\Url::to(['/company/view',
                        'id' => $companyGame->company_id]));
                }
            ],
            [
                'attribute' => 'game_id',
                'format' => 'raw',
                'value' => function(\common\models\CompanyGame $companyGame) {
                    return Html::a($companyGame->game->name, \yii\helpers\Url::to(['/game/view',
                        'id' => $companyGame->game_id]));
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
