    <?php

use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\GameCreateForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(null,
        ['class' => 'text-center control-label']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'style' => ['resize' => 'vertical']]) ?>

    <div class="form-group">
        <label for="w1" class="control-label">Insert game icon</label>
        <?= FileInput::widget([
            'name' => 'GameCreateForm[image]',
            'options' => [
                'accept' => 'image/*',
                'multiple' => false
            ],
            'pluginOptions' => [
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => true,
                'showUpload' => false,
            ]
        ]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'min_proc',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true])
            ->label(null, ['class' => 'text-center']) ?>

        <?= $form->field($model, 'rec_proc',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'min_video',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rec_vid',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'min_ram',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rec_ram',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'min_os',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rec_os',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'min_space',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rec_space',
            ['options' => ['class' => 'col-12 col-md-6 form-group']])->textInput(['maxlength' => true]) ?>
    </div>

    <?= $form->field($model, 'price')->input('number') ?>

    <div class="form-group field-game-release_date required">
        <label for="game-release_date" class="control-label">Release date</label>
        <?= DatePicker::widget([
            'name' => 'GameCreateForm[release_date]',
            'id' => 'game-release_date',
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]) ?>
    </div>

    <?= $form->field($model, 'amount')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
