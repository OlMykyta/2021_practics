<?php

use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Game */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$image = '\\backend\\web\\files\\img\\games\\default-image.jpg';
if(!is_file($image)) {
    $image = '\\backend\\web\\files\\img\\games\\'.$model->image;
}
?>
<div class="game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container genres">
        <? foreach ($model->genreGames as $genreGame): ?>
            <a class="badge badge-secondary" href="<?= Url::to(['/genre/view', 'id' => $genreGame->genre_id]) ?>">
                <?= $genreGame->genre->name ?>
            </a>
        <? endforeach; ?>
    </div>

    <p class="my-1">
        <?= Html::a('Update', ['update', 'id' => $model->game_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->game_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <img src="<?= $image ?>" alt="<?= $model->name ?>" class="col-md-5 col-12">
        <div class="col-md-7 col-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'game_id',
                    'name',
                    'description:ntext',
    //                'min_proc',
    //                'min_video',
    //                'min_ram',
    //                'min_os',
    //                'min_space',
    //                'rec_proc',
    //                'rec_vid',
    //                'rec_ram',
    //                'rec_os',
    //                'rec_space',
                    'price',
                    'release_date',
                    'amount',
        //            'image',
                ],
            ]) ?>
        </div>
    </div>

    <table class="table table-striped table-bordered mt-md-3">
        <thead>
            <tr>
                <th></th>
                <th>Minimal properties</th>
                <th>Recommended properties</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CPU</td>
                <td><?= $model->min_proc ?></td>
                <td><?= $model->rec_proc ?></td>
            </tr>
            <tr>
                <td>GPU</td>
                <td><?= $model->min_video ?></td>
                <td><?= $model->rec_vid ?></td>
            </tr>
            <tr>
                <td>RAM</td>
                <td><?= $model->min_ram ?></td>
                <td><?= $model->rec_ram ?></td>
            </tr>
            <tr>
                <td>Operational System</td>
                <td><?= $model->min_os ?></td>
                <td><?= $model->rec_os ?></td>
            </tr>
            <tr>
                <td>Disk</td>
                <td><?= $model->min_space ?></td>
                <td><?= $model->rec_space ?></td>
            </tr>
        </tbody>
    </table>

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="#companies" class="nav-link active" data-toggle="tab">Companies</a>
        </li>
        <li class="nav-item">
            <a href="#reviews" class="nav-link" data-toggle="tab">Reviews</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="companies">
            <table class="table">
                <thead>
                    <tr>
                        <th>Company name</th>
                        <th>Creation date</th>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($model->companyGames as $companyGame): ?>
                    <tr>
                        <td><?= Html::a(Html::encode($companyGame->company->name),
                                Url::to(['/company/view', 'id' => $companyGame->company_id])); ?></td>
                        <td><?= $companyGame->company->creation_date ?></td>
                        <td><?= $companyGame->company->country ?></td>
                    </tr>
                <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="reviews">
            <ul class="list-group">
                <? foreach($model->reviews as $review):
                    if ($review->user_id != Yii::$app->user->id): ?>
                        <li class="list-group-item">
                            <h4><?= $review->user->username ?></h4>
                            <p><?=  $review->review_text ?></p>
                            <?= StarRating::widget([
                                'name' => 'rating'.$review->review_id,
                                'value' => $review->rating,
                                'pluginOptions' => ['displayOnly' => true]
                            ]) ?>
                        </li>
                    <? endif;
                endforeach; ?>
            </ul>
        </div>
    </div>

</div>
