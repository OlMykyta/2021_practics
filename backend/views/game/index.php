<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\GameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Game', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Game-to-genre list', ['/genre-game'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Game-to-company list', ['/company-game'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'game_id',
            'name',
//            'description:ntext',
//            'min_proc',
//            'min_video',
            //'min_ram',
            //'min_os',
            //'min_space',
            //'rec_proc',
            //'rec_vid',
            //'rec_ram',
            //'rec_os',
            //'rec_space',
            'price',
            'release_date',
            'amount',
            //'image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
