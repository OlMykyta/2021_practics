<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$image = '\\backend\\web\\files\\img\\companies\\default-image.jpg';
if(!is_file(Yii::getAlias("@backend")."web\\files\\img\\companies\\'.$model->image")) {
    $image = '\\backend\\web\\files\\img\\companies\\'.$model->image;
}
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->company_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->company_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <img src="<?= $image ?>" alt="<?= $model->name ?>" class="col-md-5 col-sm-12">
        <div class="col-md-7 col-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'company_id',
                    'name',
                    'description:ntext',
                    'creation_date',
                    'country',
            //            'image',
                ],
            ]) ?>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>Game Name</th>
            <th>Price</th>
            <th>Release Date</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->companyGames as $companyGame): ?>
            <tr>
                <td><?= Html::a(Html::encode($companyGame->game->name),
                        Url::to(['/game/view', 'id' => $companyGame->game_id])); ?></td>
                <td><?= $companyGame->game->price ?></td>
                <td><?= $companyGame->game->release_date ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

</div>
