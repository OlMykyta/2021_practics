<?php

use common\models\Game;
use common\models\Genre;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GenreGame */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="genre-game-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'genre_id')->dropDownList(ArrayHelper::map(Genre::find()->all(),
    'genre_id', 'name')) ?>

    <?= $form->field($model, 'game_id')->dropDownList(ArrayHelper::map(Game::find()->all(),
    'game_id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
