<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GenreGame */

$this->title = 'Update Genre Game: ' . $model->genre_game_id;
$this->params['breadcrumbs'][] = ['label' => 'Genre Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->genre_game_id, 'url' => ['view', 'id' => $model->genre_game_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="genre-game-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
