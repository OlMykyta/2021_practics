<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\GenreGameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Genre Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="genre-game-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Genre Game', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'genre_game_id',
            [
                'attribute' => 'genre_id',
                'format' => 'raw',
                'value' => function(\common\models\GenreGame $genreGame) {
                    return Html::a($genreGame->genre->name, \yii\helpers\Url::to(['/genre/view',
                        'id' => $genreGame->genre_id]));
                }
            ],
            [
                'attribute' => 'game_id',
                'format' => 'raw',
                'value' => function(\common\models\GenreGame $genreGame) {
                    return Html::a($genreGame->game->name, \yii\helpers\Url::to(['/game/view',
                        'id' => $genreGame->game_id]));
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
