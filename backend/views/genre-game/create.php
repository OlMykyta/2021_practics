<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GenreGame */

$this->title = 'Create Genre Game';
$this->params['breadcrumbs'][] = ['label' => 'Genre Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="genre-game-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
