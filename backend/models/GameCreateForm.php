<?php


namespace backend\models;


use Yii;
use yii\base\Model;
use yii\web\UploadedFile;



class GameCreateForm extends Model
{
    /** @var string */
    public $name;
    /** @var string */
    public $description;
    /** @var string */
    public $min_proc;
    /** @var string */
    public $min_video;
    /** @var string */
    public $min_ram;
    /** @var string */
    public $min_os;
    /** @var string */
    public $min_space;
    /** @var string */
    public $rec_proc;
    /** @var string */
    public $rec_vid;
    /** @var string */
    public $rec_ram;
    /** @var string */
    public $rec_os;
    /** @var string */
    public $rec_space;

    /** @var UploadedFile|null $image */
    public $image;

    /** @var int */
    public $price;

    /** @var string|null */
    public $release_date;
    /** @var string|null */
    public $imageName;

    /** @var int|null */
    public $amount;

    public function rules()
    {
        return [
            [['description'], 'string'],
            [['price', 'amount'], 'integer'],
            [['release_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['min_proc', 'min_video', 'rec_proc', 'rec_vid'], 'string', 'max' => 128],
            [['min_ram', 'min_space', 'rec_ram', 'rec_space'], 'string', 'max' => 30],
            [['min_os', 'rec_os'], 'string', 'max' => 50],

            [['name', 'description', 'min_proc', 'min_video', 'min_ram', 'min_os', 'min_space',
                'rec_proc', 'rec_vid', 'rec_ram', 'rec_os', 'rec_space', 'price'], 'required'],
            [['name', 'description', 'min_proc', 'min_video', 'min_ram', 'min_os', 'min_space',
                'rec_proc', 'rec_vid', 'rec_ram', 'rec_os', 'rec_space'], 'trim'],

            [['release_date', 'amount'], 'default'],

            [['image'], 'file', 'skipOnEmpty' => 'false', 'extensions' => 'png, jpg']
        ];
    }

    public function upload() {
        $oldImage = $this->imageName;
        $this->image = UploadedFile::getInstance($this, 'image');
        if($this->validate()) {
            if(!is_null($this->image)) {
                if (!is_dir(\Yii::getAlias('@backend') . $this->getDir())) {
                    mkdir(\Yii::getAlias('@backend') . $this->getDir());
                }

                if (!is_null($oldImage) && $oldImage != '') {
                    if(is_file(\Yii::getAlias('@backend') . $this->getDir().$oldImage)) {
                        unlink(\Yii::getAlias('@backend') . $this->getDir() . $oldImage);
                    }
                }

                $this->imageName = Yii::$app->security->generateRandomString() . '.' . $this->image->extension;
                $this->image->saveAs(\Yii::getAlias('@backend') . $this->getDir() . $this->imageName);
            }
            else if (is_null($oldImage)) {
                $this->imageName = 'default-image.jpg';
            }

            return true;
        }

        return false;
    }

    public static function getDir() {
        return '\\web\\files\\img\\games\\';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'game_id' => 'Game ID',
            'name' => 'Name',
            'description' => 'Description',
            'min_proc' => 'Minimal required CPU',
            'min_video' => 'Minimal required GPU',
            'min_ram' => 'Minimal required RAM',
            'min_os' => 'Minimal required OS',
            'min_space' => 'Minimal required space',
            'rec_proc' => 'Recommended required CPU',
            'rec_vid' => 'Recommended required GPU',
            'rec_ram' => 'Recommended required RAM',
            'rec_os' => 'Recommended required OS',
            'rec_space' => 'Recommended required space',
            'price' => 'Price',
            'release_date' => 'Release Date',
            'amount' => 'Amount',
            'image' => 'Image',
        ];
    }
}