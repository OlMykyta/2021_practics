<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Game;

/**
 * GameSearch represents the model behind the search form of `common\models\Game`.
 */
class GameSearch extends Game
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['game_id', 'price', 'amount'], 'integer'],
            [['name', 'description', 'min_proc', 'min_video', 'min_ram', 'min_os', 'min_space', 'rec_proc', 'rec_vid', 'rec_ram', 'rec_os', 'rec_space', 'release_date', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Game::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'game_id' => $this->game_id,
            'price' => $this->price,
            'release_date' => $this->release_date,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'min_proc', $this->min_proc])
            ->andFilterWhere(['like', 'min_video', $this->min_video])
            ->andFilterWhere(['like', 'min_ram', $this->min_ram])
            ->andFilterWhere(['like', 'min_os', $this->min_os])
            ->andFilterWhere(['like', 'min_space', $this->min_space])
            ->andFilterWhere(['like', 'rec_proc', $this->rec_proc])
            ->andFilterWhere(['like', 'rec_vid', $this->rec_vid])
            ->andFilterWhere(['like', 'rec_ram', $this->rec_ram])
            ->andFilterWhere(['like', 'rec_os', $this->rec_os])
            ->andFilterWhere(['like', 'rec_space', $this->rec_space])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
