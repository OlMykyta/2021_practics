<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CompanyGame;

/**
 * CompanyGameSearch represents the model behind the search form of `common\models\CompanyGame`.
 */
class CompanyGameSearch extends CompanyGame
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_game_id', 'company_id', 'game_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyGame::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_game_id' => $this->company_game_id,
            'company_id' => $this->company_id,
            'game_id' => $this->game_id,
        ]);

        return $dataProvider;
    }
}
