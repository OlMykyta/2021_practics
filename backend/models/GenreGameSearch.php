<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GenreGame;

/**
 * GenreGameSearch represents the model behind the search form of `common\models\GenreGame`.
 */
class GenreGameSearch extends GenreGame
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['genre_game_id', 'genre_id', 'game_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GenreGame::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'genre_game_id' => $this->genre_game_id,
            'genre_id' => $this->genre_id,
            'game_id' => $this->game_id,
        ]);

        return $dataProvider;
    }
}
