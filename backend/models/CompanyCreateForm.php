<?php


namespace backend\models;


use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


class CompanyCreateForm extends Model
{
    /* @var int */
    public $company_id;
    /* @var string */
    public $name;
    /* @var string */
    public $description;
    /* @var string|null */
    public $creation_date;
    /* @var string */
    public $country;
    /* @var string|null */
    public $imageName;

    /* @var UploadedFile|null $image */
    public $image;

    public function rules()
    {
        return [
            [['name', 'description', 'country'], 'required'],
            [['description'], 'string'],
            [['creation_date'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['country'], 'string', 'max' => 50],

            [['image'], 'file', 'skipOnEmpty' => 'false', 'extensions' => 'png, jpg'],
        ];
    }

    public function upload() {
        $oldImage = $this->imageName;
        $this->image = UploadedFile::getInstance($this, 'image');
        if($this->validate()) {
            if(!is_null($this->image)) {
                if (!is_dir(\Yii::getAlias('@backend') . $this->getDir())) {
                    mkdir(\Yii::getAlias('@backend') . $this->getDir());
                }

                if (!is_null($oldImage) && $oldImage != '') {
                    if(is_file(\Yii::getAlias('@backend') . $this->getDir().$oldImage)) {
                        unlink(\Yii::getAlias('@backend') . $this->getDir() . $oldImage);
                    }
                }

                $this->imageName = Yii::$app->security->generateRandomString() . '.' . $this->image->extension;
                $this->image->saveAs(\Yii::getAlias('@backend') . $this->getDir() . $this->imageName);
            }
            else if (is_null($oldImage)) {
                $this->imageName = 'default-image.jpg';
            }

            return true;
        }

        return false;
    }

    public static function getDir() {
        return '\\web\\files\\img\\companies\\';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'name' => 'Name',
            'description' => 'Description',
            "creation_date" => 'Creation Date',
            'country' => 'Country',
            'image' => 'Image',
        ];
    }
}