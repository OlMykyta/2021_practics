<?php

namespace backend\controllers;

use Yii;
use common\models\CompanyGame;
use backend\models\CompanyGameSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyGameController implements the CRUD actions for CompanyGame model.
 */
class CompanyGameController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Administrator', 'Moderator']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all CompanyGame models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyGameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CompanyGame model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyGame();

        if ($model->load(Yii::$app->request->post())) {
            $company_games = CompanyGame::findAll(['company_id' => $model->company_id, 'game_id' => $model->game_id]);
            $is_present = count($company_games) > 0;

            if(!$is_present && $model->save()) {
                return $this->redirect(['index']);
            }
            Yii::$app->session->setFlash('error', 'Chosen company and game are already bound.');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CompanyGame model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $company_games = CompanyGame::findAll([]);

            $is_present = false;
            foreach ($company_games as $company_game) {
                if($company_game->game_id == $model->game_id &&
                    $company_game->company_id == $model->company_id) {
                    $is_present = true;
                    break;
                }
            }
            if(!$is_present && $model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CompanyGame model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyGame model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyGame the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyGame::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
