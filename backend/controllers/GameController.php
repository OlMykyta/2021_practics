<?php

namespace backend\controllers;

use backend\models\GameCreateForm;
use Yii;
use common\models\Game;
use backend\models\GameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Game models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Game model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GameCreateForm();

        if ($model->load(Yii::$app->request->post()) && $model->upload()) {
            $game = $this->insertToModel(null, $model);
            if ($game->save()) {
                return $this->redirect(['view', 'id' => $game->game_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $game = $this->findModel($id);
        $model = $this->insertToForm($game);

        if ($model->load(Yii::$app->request->post()) && $model->upload()) {
            $game = $this->insertToModel($game, $model);
            if ($game->update()) {
                return $this->redirect(['view', 'id' => $game->game_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'game_id' => $id
        ]);
    }


    /**
     * @param Game|null $game
     * @param GameCreateForm $form
     * @return Game
     */
    private function insertToModel($game, $form) {
        if (is_null($game)) {
            $game = new Game();
        }
        $game->image =        $form->imageName;
        $game->name =         $form->name;
        $game->description =  $form->description;
        $game->min_proc =     $form->min_proc;
        $game->min_os =       $form->min_os;
        $game->min_ram =      $form->min_ram;
        $game->min_space =    $form->min_space;
        $game->min_video =    $form->min_video;
        $game->rec_proc =     $form->rec_proc;
        $game->rec_os =       $form->rec_os;
        $game->rec_ram =      $form->rec_ram;
        $game->rec_space =    $form->rec_space;
        $game->rec_vid =      $form->rec_vid;
        $game->price =        $form->price;
        $game->release_date = $form->release_date ? $form->release_date : $game->release_date;
        $game->amount =       $form->amount;

        return $game;
    }

    /**
     * @param Game $model
     * @return GameCreateForm
     */
    private function insertToForm($model) {
        $form = new GameCreateForm();
        $form->imageName =    $model->image;
        $form->name =         $model->name;
        $form->description =  $model->description;
        $form->min_proc =     $model->min_proc;
        $form->min_os =       $model->min_os;
        $form->min_ram =      $model->min_ram;
        $form->min_space =    $model->min_space;
        $form->min_video =    $model->min_video;
        $form->rec_proc =     $model->rec_proc;
        $form->rec_os =       $model->rec_os;
        $form->rec_ram =      $model->rec_ram;
        $form->rec_space =    $model->rec_space;
        $form->rec_vid =      $model->rec_vid;
        $form->price =        $model->price;
        $form->release_date = $model->release_date ? $model->release_date : $form->release_date;
        $form->amount =       $model->amount;

        return $form;
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
