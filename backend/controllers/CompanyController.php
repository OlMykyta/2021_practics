<?php

namespace backend\controllers;

use backend\models\CompanyCreateForm;
use Yii;
use common\models\Company;
use backend\models\CompanySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Administrator', 'Moderator']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyCreateForm();

        if ($model->load(Yii::$app->request->post()) && $model->upload()) {
            $company = $this->insertToModel(null, $model);
            if($company->save()) {
                return $this->redirect(['view', 'id' => $model->company_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $company = $this->findModel($id);
        $model = $this->insertToForm($company);

        if ($model->load(Yii::$app->request->post()) && $model->upload()) {
            $company = $this->insertToModel($company, $model);
            if($company->update()) {
                return $this->redirect(['view', 'id' => $company->company_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'company_id' => $id,
        ]);
    }

    /**
     * @param Company $company
     * @param CompanyCreateForm $form
     *
     * @return Company
     */
    private function insertToModel($company, $form) {
        if(is_null($company)) {
            $company = new Company();
        }
        $company->name = $form->name;
        $company->description = $form->description;
        $company->creation_date = $form->creation_date ? $form->creation_date : $company->creation_date;
        $company->country = $form->country;
        $company->image = $form->imageName;

        return $company;
    }

    /**
     * @param Company $company
     * @return CompanyCreateForm
     */
    private function insertToForm($company) {
        $form = new CompanyCreateForm();
        $form->name = $company->name;
        $form->description = $company->description;
        $form->creation_date = $company->creation_date ? $company->creation_date : $form->creation_date;
        $form->country = $company->country;
        $form->imageName = $company->image;

        return $form;
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
