<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "game".
 *
 * @property int $game_id
 * @property string $name
 * @property string $description
 * @property string $min_proc
 * @property string $min_video
 * @property string $min_ram
 * @property string $min_os
 * @property string $min_space
 * @property string $rec_proc
 * @property string $rec_vid
 * @property string $rec_ram
 * @property string $rec_os
 * @property string $rec_space
 * @property int $price
 * @property string|null $release_date
 * @property int|null $amount
 * @property string|null $image
 *
 * @property CompanyGame[] $companyGames
 * @property GenreGame[] $genreGames
 * @property Purchase[] $purchases
 * @property Review[] $reviews
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'min_proc', 'min_video', 'min_ram', 'min_os', 'min_space', 'rec_proc', 'rec_vid', 'rec_ram', 'rec_os', 'rec_space', 'price'], 'required'],
            [['description'], 'string'],
            [['price', 'amount'], 'integer'],
            [['release_date'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
            [['min_proc', 'min_video', 'rec_proc', 'rec_vid'], 'string', 'max' => 128],
            [['min_ram', 'min_space', 'rec_ram', 'rec_space'], 'string', 'max' => 30],
            [['min_os', 'rec_os'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'game_id' => 'Game ID',
            'name' => 'Name',
            'description' => 'Description',
            'min_proc' => 'Minimal required CPU',
            'min_video' => 'Minimal required GPU',
            'min_ram' => 'Minimal required RAM',
            'min_os' => 'Minimal required OS',
            'min_space' => 'Minimal required space',
            'rec_proc' => 'Recommended required CPU',
            'rec_vid' => 'Recommended required GPU',
            'rec_ram' => 'Recommended required RAM',
            'rec_os' => 'Recommended required OS',
            'rec_space' => 'Recommended required space',
            'price' => 'Price',
            'release_date' => 'Release Date',
            'amount' => 'Amount',
            'image' => 'Image',
        ];
    }

    /**
     * Gets query for [[CompanyGames]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGames()
    {
        return $this->hasMany(CompanyGame::className(), ['game_id' => 'game_id']);
    }

    /**
     * Gets query for [[GenreGames]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenreGames()
    {
        return $this->hasMany(GenreGame::className(), ['game_id' => 'game_id']);
    }

    /**
     * Gets query for [[Purchases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPurchases()
    {
        return $this->hasMany(Purchase::className(), ['game_id' => 'game_id']);
    }

    /**
     * Gets query for [[Reviews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['game_id' => 'game_id']);
    }
}
