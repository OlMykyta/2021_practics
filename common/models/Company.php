<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $company_id
 * @property string $name
 * @property string $description
 * @property string $creation_date
 * @property string $country
 * @property string|null $image
 *
 * @property CompanyGame[] $companyGames
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'creation_date', 'country'], 'required'],
            [['description'], 'string'],
            [['creation_date'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['country'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'name' => 'Name',
            'description' => 'Description',
            'creation_date' => 'Creation Date',
            'country' => 'Country',
            'image' => 'Image',
        ];
    }

    /**
     * Gets query for [[CompanyGames]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGames()
    {
        return $this->hasMany(CompanyGame::className(), ['company_id' => 'company_id']);
    }
}
