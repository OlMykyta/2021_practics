<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "genre".
 *
 * @property int $genre_id
 * @property string $name
 * @property string $description
 *
 * @property GenreGame[] $genreGames
 */
class Genre extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'genre_id' => 'Genre ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[GenreGames]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenreGames()
    {
        return $this->hasMany(GenreGame::className(), ['genre_id' => 'genre_id']);
    }
}
