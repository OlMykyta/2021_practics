<?php

namespace frontend\controllers;

use common\models\Game;
use Yii;
use common\models\Purchase;
use frontend\models\PurchaseSearch;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PurchaseController implements the CRUD actions for Purchase model.
 */
class PurchaseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Creates a new Purchase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id int Id of the game
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate($id)
    {
        $model = new Purchase();
        $game = Game::findOne(["game_id" => $id]);

        if ($model->load(Yii::$app->request->post())) {
            if($game->amount < $model->amount) {
                Yii::$app->session->setFlash("error", "Amount of games tried to buy ({$model->amount})
                 is bigger that available ({$game->amount})");
                $bought = false;
            }
            else {
                $game->amount -= abs($model->amount);
                $game->update();
                $bought = true;
            }

            $model->game_id = $game->game_id;
            $model->user_id = Yii::$app->user->id;
            if($bought)
            {
                if($model->save()) {
                    Yii::$app->session->setFlash('success', 'You have purchased a game/ games');
                    return $this->redirect(['/game/view', 'id' => $model->purchase_id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'game' => $game
        ]);
    }

    /**
     * Finds the Purchase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Purchase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Purchase::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
