<?php

namespace frontend\controllers;

use common\models\Game;
use Yii;
use common\models\Review;
use frontend\models\ReviewSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $game_id int Game's ID
     * @return mixed
     */
    public function actionCreate($game_id)
    {
        $model = new Review();
        $game = Game::findOne(['game_id' => $game_id]);

        if ($model->load(Yii::$app->request->post())) {
            if($model->rating < 1) {
                $model->rating = 1;
            }
            else if ($model->rating > 5) {
                $model->rating = 5;
            }

            $model->user_id = Yii::$app->user->id;
            $model->game_id = $game_id;

            if($model->save())
            {
                return $this->redirect(['/game/view', 'id' => $game_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'game' => $game
        ]);
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if that review isn't user's
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $game_id = $model->game_id;

        if($model->user_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->game_id = $game_id;
            $model->user_id = Yii::$app->user->id;

            if($model->update())
            {
                return $this->redirect(['view', 'id' => $model->review_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
