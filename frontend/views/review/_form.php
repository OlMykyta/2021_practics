<?php

use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Review */
/* @var $game common\models\Game */
/* @var $form yii\widgets\ActiveForm */

$image = '\\backend\\web\\files\\img\\games\\'.$game->image;
?>

<div class="review-form">

    <div class="col-md-6">
        <img src="<?= $image ?>" alt="<?= $game->name ?>" class="col-md-5 col-12">
        <div class="col-md-7 col-12">
            <?= DetailView::widget([
                'model' => $game,
                'attributes' => [
                    'name',
                    'price',
                    'release_date'
                ],
            ]) ?>
        </div>
    </div>

    <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'review_text')->textarea(['rows' => 6, 'style' => ['resize' => 'vertical']]) ?>

        <?= $form->field($model, 'rating')->widget(StarRating::className(), [
            'pluginOptions' => [
                'step' => 1,
                'type' => 'number'
            ],
            'options' => [
                'type' => 'number'
            ]
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
