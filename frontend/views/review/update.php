<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Review */

$this->title = 'Update Review on ' . $model->game->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['/game/index']];
$this->params['breadcrumbs'][] = ['label' => $model->game->name, 'url' => ['/game/view', 'id' => $model->game_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="review-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'game' => $model->game
    ]) ?>

</div>
