<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Review */
/* @var $game common\models\Game */

$this->title = 'Create Review';
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['/game/index']];
$this->params['breadcrumbs'][] = ['label' => $game->name, 'url' => ['/game/view', 'id' => $model->game_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'game' => $game
    ]) ?>

</div>
