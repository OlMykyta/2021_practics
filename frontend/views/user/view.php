<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$current_user = $model->id == Yii::$app->user->id;
?>
<div class="user-view">

    <h1 class="text-center">Profile of user <?= Html::encode($this->title) ?></h1>

    <? if($current_user): ?>
    <p>
        <?= Html::a('Update', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>
    <? endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'surname',
            'email:email',
        ],
    ]) ?>

    <h3 class="text-center">Reviews</h3>
    <table class="table">
        <thead>
            <tr>
                <th>Game</th>
                <th>Rating</th>
            </tr>
        </thead>
        <tbody>
        <? foreach ($model->reviews as $review): ?>
            <tr>
                <td><?= Html::a(Html::encode($review->game->name),
                        Url::to(['/game/view', 'id' => $review->game_id])); ?></td>
                <td><?= $review->rating ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <? if($current_user): ?>
    <h3 class="text-center">Purchases</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Game</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->purchases as $purchase): ?>
            <tr>
                <td><?= Html::a(Html::encode($purchase->game->name),
                        Url::to(['/game/view', 'id' => $purchase->game_id])); ?></td>
                <td><?= $purchase->amount ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <? endif; ?>

</div>
