<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Purchase */
/* @var $game common\models\Game */

$this->title = "Buy \"{$game->name}\"";
$this->params['breadcrumbs'][] = ['label' => $game->name, 'url' => ['/game/view', 'id' => $game->game_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'game' => $game
    ]) ?>

</div>
