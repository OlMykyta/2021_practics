<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Purchase */
/* @var $game common\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-form">

    <h3 class="text-center"><?= $game->name ?></h3>
    <h4 class="text-center">Unit price: <span id="price"><?= $game->price ?></span></h4>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amount')->input('number', ['min' => '0', 'max' => $game->amount]) ?>

    <p>Total price: <span id="total_price">0</span></p>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    let price = parseInt(document.getElementById('price').innerHTML)

    let amount = document.getElementById('purchase-amount')
    let total_price = document.getElementById('total_price')
    amount.addEventListener('input', function () {
        total_price.innerText = (price * parseInt(amount.value)).toString()
    })
</script>
