<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to the "NOVA-Gamer"!</h1>

        <p class="lead">
            Have a chance to use our resource and buy good ol' games, classical or modern, but first ensure you're
            registered.
        </p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['login']) ?>">Get started!</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Video games</h2>

                <p>
                    We have plenty of resources in order to fulfil our customers' desires. In time our store will
                    fill with propositions including new hits. Undying classics or modern masterpieces are ready
                    for get bought and bring great experience!
                </p>

                <p><a class="btn btn-default" href="<?= Url::to(['/game']) ?>">Games!</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Different genres</h2>

                <p>
                    With each new game in the industry sometimes there is a chance to create new genre, with high luck -
                    the genre of the name/series itself! Some games are so rare that their genres aren't big.
                    Stay tuned and seek games easily by genres you like.
                </p>

                <p><a class="btn btn-default" href="<?= Url::to(['/genre']) ?>">Genres!</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Giants and amateurs</h2>

                <p>
                    Different companies are working often for different communities and people. Only really big
                    crowds of developers are huge enough for everyone everywhere. Try to find games of companies you're
                    familiar with or discover new things!
                </p>

                <p><a class="btn btn-default" href="<?= Url::to(['/company']) ?>">Companies!</a></p>
            </div>
        </div>

    </div>
</div>
