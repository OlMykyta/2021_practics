<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This resource is the Internet web-store of video games that was founded in the June 14th 2021.
        Goal of this website at first was sharing ideas and communicating but very talented Internet-businessman
        taught us to develop idea into something more. As the result - we are offering you different games to buy and
        discover for yourself! You can even become our moderator, contact us via e-mail, on the
        <?= Html::a('"Contact" page.', ['contact']) ?>
    </p>
</div>
