<?php

use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CompanySearch */
/* @var $companies Company[] */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <div class="card-deck" style="margin-top: 10px">
        <? foreach ($companies as $company): ?>
            <div class="card bg-primary col-md-3 col-sm-6"
                 style="padding: 10px; border: 3px solid white; border-radius: 10px">
                <a href="<?= Url::to(['/company/view', 'id' => $company->company_id]) ?>">
                    <img src="<?= '\\backend\\web\\files\\img\\companies\\'.$company->image ?>"
                         alt="<?= $company->name ?>" class="card-img-top d-block"
                         style="max-width: 100%; margin: auto">
                </a>
                <div class="card-body">
                    <h3 class="card-title"><?= $company->name ?></h3>
                    <p class="card-text">Creation date: <?= $company->creation_date ?></p>
                    <p class="card-text">Country: <?= $company->country ?></p>
                </div>
            </div>
        <? endforeach; ?>
    </div>

    <?php Pjax::end(); ?>

</div>
