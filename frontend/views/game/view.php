<?php

use common\models\Review;
use common\models\User;
use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Game */
/* @var $review_model Review */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$image = '\\backend\\web\\files\\img\\games\\'.$model->image;
?>
<div class="game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container genres">
        <? foreach ($model->genreGames as $genreGame): ?>
            <a class="badge badge-secondary" href="<?= Url::to(['/genre/view', 'id' => $genreGame->genre_id]) ?>">
                <?= $genreGame->genre->name ?>
            </a>
        <? endforeach; ?>
    </div>

    <div class="row" style="margin-top: 10px">
        <img src="<?= $image ?>" alt="<?= $model->name ?>" class="col-md-5 col-12">
        <div class="col-md-7 col-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'description:ntext',
                    'price',
                    'release_date',
                    'amount',
                ],
            ]) ?>

            <?
            $avg = 0;
            foreach ($model->reviews as $review) {
                $avg += $review->rating;
            }
            if (count($model->reviews) > 0) $avg /= count($model->reviews)
            ?>
            <h4>Total rating:</h4>
            <?= StarRating::widget([
                'name' => 'total_rating',
                'value' => $avg,
                'pluginOptions' => ['displayOnly' => true, 'step' => 0.1]
            ]) ?>
        </div>
    </div>

    <table class="table table-striped table-bordered mt-md-3" style="margin-top: 10px">
        <thead>
        <tr>
            <th></th>
            <th>Minimal properties</th>
            <th>Recommended properties</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>CPU</td>
            <td><?= $model->min_proc ?></td>
            <td><?= $model->rec_proc ?></td>
        </tr>
        <tr>
            <td>GPU</td>
            <td><?= $model->min_video ?></td>
            <td><?= $model->rec_vid ?></td>
        </tr>
        <tr>
            <td>RAM</td>
            <td><?= $model->min_ram ?></td>
            <td><?= $model->rec_ram ?></td>
        </tr>
        <tr>
            <td>Operational System</td>
            <td><?= $model->min_os ?></td>
            <td><?= $model->rec_os ?></td>
        </tr>
        <tr>
            <td>Disk</td>
            <td><?= $model->min_space ?></td>
            <td><?= $model->rec_space ?></td>
        </tr>
        </tbody>
    </table>
    <?= Html::a('Buy game', Url::to(['/purchase/create', 'id' => $model->game_id]),
        ['class' => 'btn btn-lg btn-success', 'style' => ['display' => 'block']]) ?>

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="#companies" class="nav-link active" data-toggle="tab">Companies</a>
        </li>
        <li class="nav-item">
            <a href="#reviews" class="nav-link" data-toggle="tab">Reviews</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="companies">
            <table class="table">
                <thead>
                <tr>
                    <th>Company name</th>
                    <th>Creation date</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($model->companyGames as $companyGame): ?>
                    <tr>
                        <td><?= Html::a(Html::encode($companyGame->company->name),
                                Url::to(['/company/view', 'id' => $companyGame->company_id])); ?></td>
                        <td><?= $companyGame->company->creation_date ?></td>
                        <td><?= $companyGame->company->country ?></td>
                    </tr>
                <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="reviews">
            <?
            $reviewed = !is_null($review_model);
            if (!$reviewed) {
                echo Html::a('Add your review', Url::to(['/review/create', 'game_id' => $model->game_id]),
                ['class' => 'btn btn-primary']);
            }?>
            <ul class="list-group">
                <?
                if($reviewed): ?>
                <li class="list-group-item">
                    <h4><?= $review_model->user->username ?></h4>
                    <p><?=  $review_model->review_text ?></p>
                    <?= StarRating::widget([
                        'name' => 'rating'.$review_model->review_id,
                        'value' => $review_model->rating,
                        'pluginOptions' => ['displayOnly' => true]
                    ]) ?>
                    <?= Html::a('Edit review', ['/review/edit', 'id' => $review_model->review_id],
                        ['class' => 'btn btn-primary']) ?>
                </li>
                <? endif; ?>

            <? foreach($model->reviews as $review):
                if ($review->user_id != Yii::$app->user->id): ?>
                <li class="list-group-item">
                    <h4><?= $review->user->username ?></h4>
                    <p><?=  $review->review_text ?></p>
                    <?= StarRating::widget([
                        'name' => 'rating'.$review->review_id,
                        'value' => $review->rating,
                        'pluginOptions' => ['displayOnly' => true]
                    ]) ?>
                </li>
            <? endif;
            endforeach; ?>
            </ul>
        </div>
    </div>

</div>
