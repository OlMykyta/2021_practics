<?php

use common\models\Game;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GameSearch */
/* @var $games Game[] */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <div class="card-deck" style="margin-top: 10px">
        <? foreach ($games as $game): ?>
            <div class="card bg-success col-md-3 col-sm-6"
                 style="padding: 10px; border: 3px solid white; border-radius: 10px">
                <a href="<?= Url::to(['/game/view', 'id' => $game->game_id]) ?>">
                    <img src="<?= '\\backend\\web\\files\\img\\games\\'.$game->image ?>"
                         alt="<?= $game->name ?>" class="card-img-top d-block"
                         style="max-width: 100%; margin: auto">
                </a>
                <div class="card-body">
                    <h3 class="card-title"><?= $game->name ?></h3>
                    <p class="card-text">Release date: <?= $game->release_date ?></p>
                    <?
                    $rating = 0;
                    foreach ($game->reviews as $review) {
                        $rating += $review->rating;
                    }
                    if(count($game->reviews) > 0) $rating /= count($game->reviews)
                    ?>
                    <p class="card-text">Rating: <?= $rating ?></p>
                </div>
            </div>
        <? endforeach; ?>
    </div>

    <?php Pjax::end(); ?>

</div>
