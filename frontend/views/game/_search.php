<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GameSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'min_proc') ?>

    <?= $form->field($model, 'min_video') ?>

    <?php // echo $form->field($model, 'min_ram') ?>

    <?php // echo $form->field($model, 'min_os') ?>

    <?php // echo $form->field($model, 'min_space') ?>

    <?php // echo $form->field($model, 'rec_proc') ?>

    <?php // echo $form->field($model, 'rec_vid') ?>

    <?php // echo $form->field($model, 'rec_ram') ?>

    <?php // echo $form->field($model, 'rec_os') ?>

    <?php // echo $form->field($model, 'rec_space') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'release_date') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'image') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
