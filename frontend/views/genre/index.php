<?php

use common\models\Genre;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $genres Genre[] */

$this->title = 'Genres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="genre-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <ul class="list-group" style="margin-top: 10px">
        <? foreach ($genres as $genre): ?>
            <li class="list-group-item"><?= Html::a($genre->name, Url::to(['/genre/view', 'id' => $genre->genre_id]),
                ['style' => 'text-align: center; display: block; width: 100%']) ?>
            </li>
        <? endforeach; ?>
    </ul>

    <?php Pjax::end(); ?>

</div>
