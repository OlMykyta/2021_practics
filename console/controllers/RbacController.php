<?php


namespace console\controllers;


use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit() {
        $auth = Yii::$app->authManager;

        $user = $auth->createRole("User");
        $auth->add($user);

        $moder = $auth->createRole("Moderator");
        $auth->add($moder);

        $admin = $auth->createRole("Administrator");
        $auth->add($admin);


        $auth->addChild($moder, $user);
        $auth->addChild($admin, $moder);

        $auth->assign($admin, 1);
        $auth->assign($moder, 2);
        $auth->assign($user, 3);
    }
}